//1. Translate the other students from our boilerplate code into their own respective objects.

//2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

//3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

//4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

let studentOne = {
    name:'John',
    email:'john@mail.com',
    grades:[89, 84, 78, 88],
    computeAve(){
        return (this.grades.reduce(function(tot, curr){return tot+=curr}, 0))/this.grades.length
    },
    willPass(){
        return (this.computeAve()>85)
    },
    willPassWithHonors(){
        if(this.willPass()){
            return (this.computeAve()>=90)
        }else{
            return undefined
        }
    }
}
let studentTwo = {
    name:'Joe',
    email:'joe@mail.com',
    grades:[78, 82, 79, 85],
    computeAve(){
        return (this.grades.reduce(function(tot, curr){return tot+=curr}, 0))/this.grades.length
    },
    willPass(){
        return (this.computeAve()>85)
    },
    willPassWithHonors(){
        if(this.willPass()){
            return (this.computeAve()>=90)
        }else{
            return undefined
        }
    }

}
let studentThree = {
    name:'Jane',
    email:'jane@mail.com',
    grades:[87, 89, 91, 93],
    computeAve(){
        return (this.grades.reduce(function(tot, curr){return tot+=curr}, 0))/this.grades.length
    },
    willPass(){
        return (this.computeAve()>85)
    },
    willPassWithHonors(){
        if(this.willPass()){
            return (this.computeAve()>=90)
        }else{
            return undefined
        }
    }

}
let studentFour = {
    name:'Jessie',
    email:'jessie@mail.com',
    grades:[91, 89, 92, 93],
    computeAve(){
        return (this.grades.reduce(function(tot, curr){return tot+=curr}, 0))/this.grades.length
    },
    willPass(){
        return (this.computeAve()>85)
    },
    willPassWithHonors(){
        if(this.willPass()){
            return (this.computeAve()>=90)
        }else{
            return undefined
        }
    }

}

//5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

//6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

//7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

//8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

//9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],
    countHonorStudents(){
        return this.students.reduce((tot,curr)=>{if(curr.willPassWithHonors()){tot++}return tot},0)
    },
    honorsPercentage(){
        return (this.countHonorStudents()/this.students.length)*100
    },
    retrieveHonorStudentInfo(){
        let arr=[]
        this.students.forEach(element => {
            if(element.willPassWithHonors()){
                arr.push({email: element.email, ave: element.computeAve()})
            }
        });
        return arr
    },
    sortHonorStudentsByGradeDesc(){
        return this.retrieveHonorStudentInfo().sort((a,b)=>{b.ave-a.ave})
    }
}