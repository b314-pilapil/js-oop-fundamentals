// use an object literal: {} to create an object representing a user
    // encapsulation
    // whenever we add properties/methods to an object, we are performing ENCAPSULATION
    // the organization of information(as properties) and behaviors(as methods) to belong to the object that encapsulates them
    // the scope of encapsulation is denoted by object literals

// function Student(name, email, grades){
//     this.name=name,
//     this.email=email,
//     this.grades=grades
//     this.login= function(){
//         console.log(`${this.email} has logged in`);
//     },
//     this.logout=function(){
//         console.log(`${this.email} has logged out`);
//     }
//     this.listGrades=function(){
//         this.grades.forEach(grade => {
//             console.log(grade);
//         })
//     }
// }


let studentOne = {
    name: 'John',
    email:'john@mail.com',
    grades: [89, 84, 78, 88],
    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listGrades(){
        console.log(`studentOne's grades is ${this.grades}`)
    },
    getAverage(){
        return (this.grades.reduce(function(tot, curr){return tot+=curr}, 0))/this.grades.length
    },
    hasPassed(){
        return (this.getAverage()>85)
    },
    willPassWithHonors(){
        return (this.getAverage()>=90 && this.hasPassed())
    }

}
// console.log(studentOne)
// console.log(`studentOne's name is ${studentOne.name}`)
// console.log(`studentOne's email is ${studentOne.email}`)
// console.log(`studentOne's grades is ${studentOne.grades}`)

// let studentTwo = {
//     name: 'Joe',
//     email:'joe@mail.com',
//     grades: [78, 82, 79, 85]
// }
// console.log(studentTwo)

// let studentThree = {
//     name: 'Jane',
//     email:'jane@mail.com',
//     grades: [87, 89, 91, 93]
// }
// console.log(studentThree)

// let studentFour = {
//     name: 'Jessie',
//     email:'jessie@mail.com',
//     grades: [91, 89, 92, 93]
// }
// console.log(studentFour)