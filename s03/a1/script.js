// in js, classes can be craetedusing the class keyword and {}
//  class <Name>{
// }

class Student{
    // to enable students instanciated from this class to have distince name & emails, the constructor must be able to accept name & email args hich will be used to set the values of the obejct's corresponding properties
    constructor(name,email, grades){
        this.name=name
        this.email=email
        this.grades=(grades.length==4 && grades.every(x=> (x>0 && x<100)))?grades:undefined
    }
    login(){
        console.log(`${this.email} has logged in`)
        return this
    }
    logout(){
        console.log(`${this.email} has logged out`)
        return this
    }
    listGrades(){
        console.log(`${this.email} grades are: ${this.grades}`)
        return this
    }
    computeAve() {
        let sum = 0;
        if(this.grades!=undefined)
            this.grades.forEach(grade => sum += grade);
        this.gradeAve = sum / 4;
        return this;
    }
    willPass() {
        this.passed = this.computeAve().gradeAve >= 85;
        return this;
    }
    willPassWithHonors() {
        if (this.passed) {
          this.passedWithHonors = this.gradeAve >= 90;
        } else {
          this.passedWithHonors = false;
        }
        return this;
      }
    // The login() method logs a message to the console indicating that the student with the specific email has logged in.
    // The logout() method logsa message to the console indicating that the student with the specific email has logged out.
    // The listGrades() method logs a message to the console displaying the name of the student and their quarterly grade averages.
    // The computeAve() method calculates the average of the student's grades by iterating over the this.grades array and summing up all the grades.
    // The result is stored in the this.gradeAve property.

    // The willPass() method determines if the student passed or failed based on their average grade. It calls the computeAve() method to calculate the average grade and then checks if the gradeAve property is greater than or equal to 85. The result is stored in the this.passed property.

    // The willPassWithHonors() method determines if the student passed with honors based on their average grade. It first checks if the student passed (this.passed is true) and then checks if the gradeAve property is greater than or equal to 90. The result is stored in the this.passedWithHonors property.
}
// instance
let studentOne = new Student('John', 'john@mail.com', [89, 84, 78]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

// 1) What is the blueprint where objects are created from?
// Answer: prototype
// 2) What is the naming convention applied to classes?
// Answer: CamelCase
// 3) What keyword do we use to create objects from a class?
// Answer: new
// 4) What is the technical term for creating an object from a class?
// Answer: instantiate
// 5) What class method dictates HOW objects will be created from that class?
// Answer: constructor