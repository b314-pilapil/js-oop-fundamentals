// Creating an Object using Object literal notation
const person = {
    name: 'John Doe',
    age: 30,
    profession: 'Engineer',
    greet: function() {
      console.log(`Hello, my name is ${this.name} and I am ${this.age} years old.`);
    }
  };
  
  console.log(person.name); // Output: John Doe
  console.log(person['age']); // Output: 30

//   Using the constructor function to create 'Person' Objects
function Person(name, age){
    this.name = name
    this.age=age
    this.greet = function(){
        console.log(`Hello my name is ${this.name}`)
    }
}

// Cerating instances of 'Person' using the new keyword

let person1 = new Person('John Doe',30)
let person2 = new Person('Jane Smith',25)


// Constructors provide a way to create multiple instances of an object with shared properties and methods. They allow for the creation of similar objects without duplicating code.

// Object literals are useful for creating single instances of an object with specific properties and values.

// The main differences between constructors and object literals are as follows:

// Constructors:
    // Require the use of the new keyword to create instances.
    // Utilize the this keyword to refer to the current instance being created.
    // Allow for the definition of shared methods and properties using the prototype.

// Object Literals:
    // Are created directly using curly braces {}.
    // Define properties and values within the object literal itself.
    // Are suitable for creating individual instances of objects with specific values.

class Person2{
    constructor(name,age){
        this.name=name
        this.age=age
    }
    greet(){
        console.log(`Hello my name is ${this.name}`)
    }
}

// Creating instances
const personA = new Person2('Jogn Doe',30)

// Classes are a more modern approach introduced in ECMAScript 2015 (ES6) for creating objects in JavaScript.

// Classes provide a cleaner syntax and a clearer separation between constructors, methods, and properties.

// The class keyword is used to define a class, and the constructor method is used for initializing object properties.

// Methods defined within a class are automatically shared among instances.

// Classes offer the ability to implement inheritance and other advanced object-oriented programming concepts.

class Rectangle{
    constructor(width, height){
        this.width=width
        this.height=height
    }
    getArea(){
        return this.width*this.height
    }
    getPerimeter(){
        return 2* (this.width+this.height)
    }
}
const regtangle1 = new Rectangle(5,10)

// Object literal
const car1 = {
    brand: 'Toyota',
    model: 'Camry',
    year: 2021,
    startEngine() {
        console.log(`Starting the engine of ${this.brand} ${this.model}`);
    },
};
car1.startEngine()

// Constructor function
function Car(brand, model, year) {
    this.brand = brand;
    this.model = model;
    this.year = year;
}

Car.prototype.startEngine = function(){
    console.log('starting engineee')
}

const car2 = new Car('Honda', 'Civic', 2022)

// More on Prototypes and Inheritance discussion
    // Understanding of prototypes, prototype chains, and how to implement inheritance in JavaScript using both constructor functions and classes. 
    // Create objects with shared behavior, extend functionality, and build hierarchical relationships between objects.

// Step 1: ceating a prototype object using object literals
const personPrototype={
    introduce(){
        console.log(`Hi my name is ${this.name}`)
    },

}

// In this step, we create a personPrototype object using object literals. The personPrototype serves as a blueprint or template for creating objects with shared behavior. It contains a single method introduce(), which will be shared among all objects created from this prototype.

// Step 2: Creating an object using the prototype object
const personX = Object.create(personPrototype)
personX.name='John Doe'
personX.introduce()
// Here, we create an object personX using the Object.create() method. By passing in personPrototype as the argument, we establish a prototype link between person1 and personPrototype

// Step 3: Creating a constructor function and adding methods to the prototype

function Person3(name, age){
    this.name=name
    this.age-age
}
Person3.prototype.introduce = function(){
	console.log(`Hello, my name is ${this.name}`);
}

// In this step, we define a constructor function Person. Constructors are used to create objects with specific properties and behaviors. Inside the Person constructor, we assign the name and age values to the newly created object using the this keyword. We then add the introduce() method to the prototype of the Person constructor using Person.prototype. By doing this, all objects created from the Person constructor will share the same introduce() method.

// Step 4: Creating an object using constructor function
const pers3 = new Person3('a', 1)
pers3.introduce()

// Step 5: Creating a child onject with a specific prototype
const employeeProtoytpe= Object.create(personPrototype)

employeeProtoytpe.jobTitle = ''

const employee = Object.create(employeeProtoytpe)
employee.name='mike '
employee.age = 35
employee.jobTitle='softeng'
employee.introduce()

// In this step, we create an employeePrototype object using Object.create() and set its prototype to personPrototype. This establishes a prototype chain where employeePrototype inherits from personPrototype. We also add a jobTitle property specific to employees to the employeePrototype. Then, we create an employee1 object using Object.create() and assign employeePrototype as its prototype. We provide the name, age, and jobTitle values to employee1 and invoke the introduce() method. Since employee1 doesn't have its own introduce() method, it traverses the prototype chain and finds the method in personPrototype.

// Step 6: Interiting from a parent constructor using extends keyword
class Employee extends Person2{
    constructor(name, age, jobTitle){
        super(name,age)
        this.jobTitle=jobTitle
    }
    introduce(){
        console.log(`my name is ${this.name}. my job is ${this.jobTitle}`)
    }
}

const E1 = new Employee('a', 1, 'b')

// In this step, we define a child class Employee that extends the parent class Person using the extends keyword. The extends keyword allows the child class to inherit properties and methods from the parent class. Inside the child class's constructor, we call super(name, age) to invoke the parent constructor and set the name and age values. We also add a jobTitle property specific to employees.