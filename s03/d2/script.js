// Object-oriented programming (OOP) is a programming paradigm that organizes code into objects, which are instances of classes.

// Encapsulation
    // Encapsulation is the practice of bundling data and methods that operate on that data within a single entity, known as a class. It allows for data hiding and provides a clear interface for interacting with the object.

class Circle{
    constructor(radius){
        this.radius=radius
    }
    getArea(){
        return Math.PI* this.radius**2
    }
}
const myCircle = new Circle(5)
console.log(myCircle.getArea())

// Inheritance
    // Inheritance allows a class (subclass) to inherit properties and methods from another class (parent class). It promotes code reusability and the creation of specialized classes.

class Animal{
    constructor(name){
        this.name=name
    }
    eat(){
        console.log((`${this.name} is eating.`))
    }
}
class Dog extends Animal{
    bark(){
        console.log((`${this.name} is barking.`))
    }
}
const myDog= new Dog('Buddy')
myDog.eat()
myDog.bark()

// Polymorphism
    // Polymorphism allows objects of different classes to be treated as objects of a common superclass. It enables code flexibility and dynamic behavior based on the specific object type.

class Shape{
    getArea(){
        return 0;
    }
}
class Circle2 extends Shape{
    constructor(radius){
        super()
        this.radius=radius
    }
    getArea(){
        return Math.PI * this.radius**2
    }
}
class Rectangle extends Shape{
    constructor(width, height){
        super()
        this.width=width
        this.height=height
    }
    getArea(){
        return this.width*this.height
    }
}
const shapes = [new Circle2(5), new Rectangle(10, 20)]
shapes.forEach(element => {
    console.log(element.getArea())
});