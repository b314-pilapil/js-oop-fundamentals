/*
1) How do you create arrays in JS?

    using [] (array literal) or instanciating a new Array object,
    let array1 = []
    let array2 = new Array()

2) How do you access the first character of an array?

    access index 0 of the array,
    array[0]

3) How do you access the last character of an array?

    get the length, subtract 1 and use it as the index,
    array[array.length-1]

4) What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.

    indexOf() method

5) What array method loops over all elements of an array, performing a user-defined function on each iteration?

    forEach() method

6) What array method creates a new array with elements obtained from a user-defined function?

    slice() method

7) What array method checks if all its elements satisfy a given condition?

    every() method

8) What array method checks if at least one of its elements satisfies a given condition?

    some() method


9) True or False: array.splice() modifies a copy of the array, leaving the original unchanged.

    False

10) True or False: array.slice() copies elements from original array and returns them as a new array.

    True

*/

// Function Coding

const students = ['John', 'Joe', 'Jane', 'Jessie']

// 1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.

function addToEnd(arr, elem){
    if(typeof elem === 'string'){
        arr.push(elem)
        return arr
    }else{
        return 'error - can only add strings to the array'
    }
}

// Output
// addToEnd(students, "Ryan"); //["John", "Joe", "Jane", "Jessie", "Ryan"]
// addToEnd(students, 045); //"error - can only add strings to an array"

/* 
2) Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.
*/

function addToStart(arr, elem){
    if(typeof elem === 'string'){
        arr.unshift(elem)        
        return arr
    }else{
        return 'error - can only add strings to the array'
    }
}
// addToStart(students, "Tess"); //["Tess", "John", "Joe", "Jane", "Jessie", "Ryan"]
//validation check
// addToStart(students, 033); //"error - can only add strings to an array"

/*
3) Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.*/

function elementChecker(arr, elem){
    if(arr.length>0){
        return arr.includes(elem)
    }else{
        return 'error - passed in array is empty"'
    }
}

//test input
// elementChecker(students, "Jane"); //true
//validation check
// elementChecker([], "Jane"); //"error - passed in array is empty"

/*
4) Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:


if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
if every element in the array ends in the passed in character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.
*/

function checkAllStringsEnding(arr, elem){
    if(arr.length==0){
        return "error - array must NOT be empty"
    }else if(typeof elem !== 'string'){
        return "error - 2nd argument must be of data type string"
    }else if(arr.some(e => typeof e !== 'string')){
        return "error - all array elements must be strings"
    }else if(elem.length!==1){
        return "error - 2nd argument must be a single character"
    }else{
        return (arr.every(e => e[e.length-1]===elem))
    }
}

//test input
// checkAllStringsEnding(students, "e"); //false
//validation checks
// checkAllStringsEnding([], "e"); //"error - array must NOT be empty"
// checkAllStringsEnding(["Jane", 02], "e"); //"error - all array elements must be strings"
// checkAllStringsEnding(students, 4); //"error - 2nd argument must be of data type string"
// checkAllStringsEnding(students, "el"); //"error - 2nd argument must be a single character"

/*
5) Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.
*/

function stringLengthSorter(arr){
    if(arr.some(e => typeof e !== 'string')){
        return "error - all array elements must be strings"
    }else{
        arr.sort((a,b)=> a.length-b.length)
        return arr
    }
}

//test input
// stringLengthSorter(students); //["Joe", "Tess", "John", "Jane", "Ryan", "Jessie"]
//validation check
// stringLengthSorter([037, "John", 039, "Jane"]); //"error - all array elements must be strings"

/*
6) Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:


if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
return the number of elements in the array that start with the character argument, must be case-insensitive

Use the students array and the character "J" as arguments when testing.
*/

function startsWithCounter(arr, elem){
    if(arr.length==0){
        return "error - array must NOT be empty"
    }else if(arr.some(j => typeof j !== 'string')){
        return "error - all array elements must be strings"   
    }else if(typeof elem !== 'string'){
        return "error - 2nd argument must be of data type string"
    }else if(elem.length!==1){
        return "error - 2nd argument must be a single character"
    }else{
        return arr.reduce((tot, curr)=>{
            if(curr[0].toLowerCase()==elem){
                tot++
            }
            return tot
        }, 0)
    }
}

//test input
// startsWithCounter(students, "j"); //4

/*
7) Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:


if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
return a new array containing all elements of the array argument that contain the string argument in it, must be case-insensitive

Use the students array and the string "jo" as arguments when testing.
*/
function likeFinder(arr, elem){
    if(arr.length==0){
        return "error - array must NOT be empty"
    }else if(arr.some(j => typeof j !== 'string')){
        return "error - all array elements must be strings"
    }else if(typeof elem!== 'string'){
        return "error - 2nd argument must be of data type string"
    }else{
        return arr.filter(j => j.toLowerCase().startsWith(elem))
    }
}
//test input
// likeFinder(students, "jo"); //["Joe", "John"]

/*
8) Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.
*/
function randomPicker(students){
    return students[Math.floor(Math.random()*students.length)]
}

//test input
// randomPicker(students); //"Ryan"
// randomPicker(students); //"John"
// randomPicker(students); //"Jessie"


