// // Advantages of OOP in JS
//     // Modularity and Reusability: OOP allows you to create reusable objects or classes, making it easier to organize and maintain code. Objects can be used in multiple parts of the program, promoting code reuse and reducing duplication.

//     // Code Organization: OOP provides a structured way to organize code. Classes serve as blueprints for creating objects, helping break down complex problems into manageable parts and making code easier to understand.

//     // Encapsulation and Abstraction: OOP encourages bundling data and behavior together in objects. This hides internal details and exposes only necessary information through methods, promoting cleaner code and reducing complexity.

// // non OOP
//     // Student 1
// let student1Name = 'John';
// let student1Email = 'john@mail.com';
// let student1Grades = [89, 84, 78, 88];

//     // Student 2
// let student2Name = 'Joe';
// let student2Email = 'joe@mail.com';
// let student2Grades = [78, 82, 79, 85];

// // Function to calculate average grade
// function calculateAverage(grades) {
//     let sum = 0;
//     for (let grade of grades) {
//         sum += grade;
//     }
//     return sum / grades.length;
// }

//   // Function to check if a student will pass
// function willPass(average) {
//     return average >= 70;
// }

//   // Function to check if a student will pass with honors
// function willPassWithHonors(average) {
//     return average >= 90;
// }

// // aveG stu1
// let student1Average= calculateAverage(student1Grades)

// // Check if student 1 will pass and pass with honors
// let student1Pass = willPass(student1Average);
// let student1PassWithHonors = willPassWithHonors(student1Average);

// // Calculate average grade for student 2
// let student2Average = calculateAverage(student2Grades);

// // Check if student 2 will pass and pass with honors
// let student2Pass = willPass(student2Average);
// let student2PassWithHonors = willPassWithHonors(student2Average);

// // Print results
// console.log(student1Name, 'Pass:', student1Pass, ', Pass with Honors:', student1PassWithHonors);
// console.log(student2Name, 'Pass:', student2Pass, ', Pass with Honors:', student2PassWithHonors);

// OOP ver
// class Student{
//     constructor(name, email, grades){
//         this.name=name,
//         this.email=email,
//         this.grades=grades
//     }
//     calculateAverage() {
//         let sum = 0;
//         for (let grade of this.grades) {
//             sum += grade;
//         }
//         return sum / this.grades.length;
//     }
//     willPass() {
//         return this.calculateAverage() >= 70;
//     }
//     willPassWithHonors() {
//         return this.calculateAverage() >= 90;
//     }
// }
// // Create student objects
// let student1 = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
// let student2 = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);

// // Check if students will pass and pass with honors
// let student1Pass = student1.willPass();
// let student1PassWithHonors = student1.willPassWithHonors();
// let student2Pass = student2.willPass();
// let student2PassWithHonors = student2.willPassWithHonors();

// // Print results
// console.log(student1.name, 'Pass:', student1Pass, ', Pass with Honors:', student1PassWithHonors);
// console.log(student2.name, 'Pass:', student2Pass, ', Pass with Honors:', student2PassWithHonors);

/*
Student Class
*/

class Student {
    constructor(name, email, grades){
        this.name = name;
        this.email = email;
        if(grades.length === 4){
            if(grades.every(grade => grade >= 0 && grade <= 100)){
                this.grades = grades;
            } else {
                this.grades = undefined;
            }
        } else {
            this.grades = undefined;
        }
        this.gradeAve = undefined;
        this.passed = undefined;
        this.passedWithHonors = undefined;
    }

    login(){
        console.log(`${this.email} has logged in`);
        return this;
    }
    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    }
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;
    }
    computeAve(){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        this.gradeAve = sum/4;
        return this;
    }
    willPass() {
        this.passed = this.computeAve().gradeAve >= 85 ? true : false;
        return this;
    }
    willPassWithHonors() {
        if (this.passed) {
            if (this.gradeAve >= 90) {
                this.passedWithHonors = true;
            } else {
                this.passedWithHonors = false;
            }
        } else {
            this.passedWithHonors = false;
        }
        return this;
    }
}

class Section {
    constructor(name){
        // Initialize the Section object with a name and an empty array of students.
        this.name = name;
        this.students = [];
        // Initialize honorStudents and honorsPercentage as undefined.
        this.honorStudents = undefined; //int
        this.honorsPercentage = undefined;
    }

    addStudent(name, email, grades){
        // Method for adding a student to this section.
        // Create a new Student object with the provided details and grades,
        // and add it to the students array.
        this.students.push(new Student(name, email, grades));
        // Return the Section object itself to allow method chaining.
        return this;
    }

    countHonorStudents(){
        // Method for computing the number of honor students in the section.
        let count = 0;
        // Iterate through each student in the students array.
        this.students.forEach(student => {
            // Check if the student passed with honors.
            if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
                // If the student passed with honors, increment the count.
                count++;
            }
        })
        // Update the honorStudents property with the final count.
        this.honorStudents = count;
        // Return the Section object itself to allow method chaining.
        return this;
    }

    computeHonorsPercentage(){
        // Method for computing the percentage of honor students in the section.
        // Calculate the honors percentage based on the honorStudents count and the total number of students.
        this.honorsPercentage = (this.honorStudents / this.students.length) * 100;
        // Return the Section object itself to allow method chaining.
        return this;
    }
}


/*

1) Create a new instance of the Section class named section1A
2) Add students to the section1A using the addStudent method:
  2.1) Add a student named John with email 'john@mail.com' and grades [89, 84, 78, 88]:
  2.2)Add a student named Joe with email 'joe@mail.com' and grades [78, 82, 79, 85]:
  2.3) Add a student named Jane with email 'jane@mail.com' and grades [87, 89, 91, 93]:
  2.4)Add a student named Jessie with email 'jessie@mail.com' and grades [91, 89, 92, 93]:
3. Count the number of honor students in the section using the countHonorStudents method:
4. Compute the percentage of honor students in the section using the computeHonorsPercentage method:
5. Display the results:
  5.1)Print the number of honor students:
  5.2) Print the percentage of honor students:
*/
let section1A = new Section('section1A')

section1A.addStudent('John','john@mail.com', [89, 84, 78, 88])
section1A.addStudent('Joe','joe@mail.com', [78, 82, 79, 85])
section1A.addStudent('Jane','jane@mail.com', [87, 89, 91, 93])
section1A.addStudent('Jessie','jessie@mail.com', [91, 89, 92, 93])

section1A.countHonorStudents()
section1A.computeHonorsPercentage()

console.log(section1A.honorStudents)
console.log(section1A.honorsPercentage)